# Computer Graphics CSCI 441 Syllabus

**NOTE: This is a live document and is subject to change throughout the semester. **

Computer graphics is a field of computer science that involves generating visual content.
Applications of computer graphics includes modeling and animation software, CAD design
software, image manipulation packages such as Photoshop or Illustrator, video games,
medical visualisation tools and much, much more. In this class, we’ll be going over some of
the fundamentals of computer graphics from transformations and camera projections, to light
and shading models, to simulations that approximate phenomena in the real world. We’ll be
using C++, Qt and OpenGL to explore practical, realtime, graphics programming while learning 
the theory behind it so you can apply it to other environments.

## Assignments

There will be many lab assignments, that are designed to give you a better understanding of
the topics taught in lecture. Code will be provided to get you up and running with the labs,
which you can work on in pairs or a maximum of 3 people. You must demo your labs to me in
class or office hours or push your code up to Bitbucket in a repository you've shared with me prior to class on the due date. There will also be a few major programming assignments as well. The
program assignments will take significantly more time than the labs and are designed to
inspire individual creativity and self expression. I’ll try to keep lectures as short as possible to
give you time in class to work on your assignments with me available to ask questions.
All assignments and other resources for the class will be made available via Bitbucket:

https://bitbucket.org/msucsc441spring2016/

## Meeting Times

Tuesdays and Thursdays 8:00 a.m. - 9:15 a.m.
in EPS 254

## Instructor

John Allwine

**Office hours**

Tuesdays and Thursdays 9:30 a.m. - 10:45 a.m.  or by appointment.
in EPS 259

## Textbook

* [Foundations of 3D Computer Graphics](http://www.amazon.com/Foundations-Computer-Graphics-Steven-Gortler/dp/0262017350/), (referred to as Gortler in recommended reading below)
is optional, but recommended.
* [OpenGL Programming Guide, 8th Edition](http://www.amazon.com/OpenGL-Programming-Guide-Official-Learning/dp/0321773039) (referred to as GL Guide in recommended reading below)
is also recommended, but optional.

## Online Resources

* [OpenGL Reference Pages](https://www.opengl.org/sdk/docs/man/)
* [Qt 5 Documentation](http://doc.qt.io/qt-5/)
* [C++ Documentation](http://www.cplusplus.com/doc/tutorial/)

## Class schedule

The lecture schedule is subject to change throughout the semester, but here is
the current plan. Assignments and due dates will be updated as they're assigned in class.

### January

| Date     | Description                                       | Assigned     | Due       | Recommended Reading               |
|----------|---------------------------------------------------|--------------|-----------|-----------------------------------|
| 1/14     | Intro - applications of computer graphics         |              |           |                                   |
| 1/19     | C++ overview / git basics                         |  [Lab 0](https://bitbucket.org/msucsc441spring2016/labs/src/1a15dc11bac84f384ad2be661a19ab06f8d47a3f/lab0)       |           |                                   |
| 1/21     | Overview of graphics pipeline                     |              |           | Gortler p3-8, GL Guide p10-14     |
| 1/26     | Rasterization / Barycentric coordinates / Color   |  [Lab 1](https://bitbucket.org/msucsc441spring2016/labs/src/1a15dc11bac84f384ad2be661a19ab06f8d47a3f/lab1)       |  [Lab 0](https://bitbucket.org/msucsc441spring2016/labs/src/1a15dc11bac84f384ad2be661a19ab06f8d47a3f/lab0)    |                                   |
| 1/28     | OpenGL basics / 2D coordinate systems             |              |           | GL Guide p16-30                   |
   
### February

| Date     | Description                                               | Assigned     | Due       | Recommended Reading               |
|----------|-----------------------------------------------------------|--------------|-----------|-----------------------------------|
| 2/2      | Graphics math - vectors and matrices                      | [Lab 2](https://bitbucket.org/msucsc441spring2016/labs/src/1a15dc11bac84f384ad2be661a19ab06f8d47a3f/lab2)        |  [Lab 1](https://bitbucket.org/msucsc441spring2016/labs/src/1a15dc11bac84f384ad2be661a19ab06f8d47a3f/lab1)    | Gortler p9-34, [Vector and matrix demos](http://freakinsweetapps.com/Teaching/VectorMatrixMath)                |
| 2/4      | Orthographic projection / OpenGL primitives               |              |           |                                   |
| 2/9      | Qt / OpenGL / C++ Lab Overview and Program 1 Assigment    | [Lab 3](https://bitbucket.org/msucsc441spring2016/labs/src/1a15dc11bac84f384ad2be661a19ab06f8d47a3f/lab3/), [Program 1](https://bitbucket.org/msucsc441spring2016/programs/src/e174e50a4f4d3ee47314801f405484b51861559e/program1/)       |           |                                   |
| 2/11     | Lab day   |              |  [Lab 2](https://bitbucket.org/msucsc441spring2016/labs/src/1a15dc11bac84f384ad2be661a19ab06f8d47a3f/lab2)    |                                   |
| 2/16     | Model, View, Projection Matrices   |  [Lab 4](https://bitbucket.org/msucsc441spring2016/labs/src/49a2f62fe7653c0b716ec350a462af577bc07fcc/lab4/?at=master)           |  [Lab 3](https://bitbucket.org/msucsc441spring2016/labs/src/1a15dc11bac84f384ad2be661a19ab06f8d47a3f/lab3)    |  Gortler p35-43 (Chapter 5)                                 |
| 2/18     | Transform Matrices - Translation, Rotation and Scale |              |      |                                   |
| 2/23     | Perspective Projection and the Depth Buffer                                         |              |       |  Gortler p89-108 (Chapters 10-11)                                 |
| 2/25     | Lab day                                                    |              |   [Lab 4](https://bitbucket.org/msucsc441spring2016/labs/src/49a2f62fe7653c0b716ec350a462af577bc07fcc/lab4/?at=master)        |                                   |

### March

| Date     | Description                                 | Assigned     | Due       | Recommended Reading |
|----------|---------------------------------------------|--------------|-----------|---------------------|
| 3/1      |  Virtual Trackball                          | [Lab 5](https://bitbucket.org/msucsc441spring2016/labs/src/965c4fbc3e2cb4ee78c692bd362de740094be4c6/lab5/?at=master)             |  [Program 1](https://bitbucket.org/msucsc441spring2016/programs/src/e174e50a4f4d3ee47314801f405484b51861559e/program1) | |
| 3/3      | Lab Day                       |              |           | |
| 3/8      | Materials and Lights                       |  [Lab 6](https://bitbucket.org/msucsc441spring2016/labs/src/e9c7ac2a4fed7bc90a32b41ced0ba9b0494f31bc/lab6/?at=master)            |  [Lab5](https://bitbucket.org/msucsc441spring2016/labs/src/965c4fbc3e2cb4ee78c692bd362de740094be4c6/lab5/?at=master)         | Gortler p 127-136 (Chapter 14) |
| 3/10     | Hierarchical Modeling       |   [Program 2](https://bitbucket.org/msucsc441spring2016/programs/src/3009783323306d987f73a2ff0ce056d403bb8bdf/program2/?at=master)           |           | |
| 3/15     | Spring break - no class                     |              |           | |
| 3/17     | Spring break - no class                     |              |           | |
| 3/22     | Review                      |              |  [Lab 6](https://bitbucket.org/msucsc441spring2016/labs/src/e9c7ac2a4fed7bc90a32b41ced0ba9b0494f31bc/lab6/?at=master)         | |
| 3/24     | OpenGL Element Arrays / Smooth vs Flat shading                            |   Midterm on D2L          |           | |
| 3/29     | Midterm Review / Open Lab                                   |  [Lab 7](https://bitbucket.org/msucsc441spring2016/labs/src/002daacecf77d7d663c6925893f900914a2a8eb2/lab7/?at=master)            |    Midterm on D2L before class       | |
| 3/31     |  Textures / Mipmaps                        |              |           | Gortler p137-148 (Chapter 15)|

### April

| Date     | Description                                 | Assigned     | Due       | Recommended Reading |
|----------|---------------------------------------------|--------------|-----------|---------------------|
| 4/5      | Animation basics                        |  [Lab 8](https://bitbucket.org/msucsc441spring2016/labs/src/04304917626098976d186652fbb7508e190f4a82/lab8/?at=master)            |  [Program 2](https://bitbucket.org/msucsc441spring2016/programs/src/3009783323306d987f73a2ff0ce056d403bb8bdf/program2/?at=master) AND [Lab 7](https://bitbucket.org/msucsc441spring2016/labs/src/002daacecf77d7d663c6925893f900914a2a8eb2/lab7/?at=master)       | Gortler p239-246 (Chapter 23) |
| 4/7      | First person camera                               |  [Program 3](https://bitbucket.org/msucsc441spring2016/programs/src/fa1445b330fdd8b48c2d74b15f39eb04fc4d9617/program3/?at=master)            |           | |
| 4/12     | Animating Orientation / Quaternions         |  [Lab 9](https://bitbucket.org/msucsc441spring2016/labs/src/ab65b61a95f3ea4bba89b01f50aa83e53e77f11c/lab9/?at=master)            |   [Lab 8](https://bitbucket.org/msucsc441spring2016/labs/src/04304917626098976d186652fbb7508e190f4a82/lab8/?at=master)        | Gortler p59-72 (Chapter 7) |
| 4/14     | Bezier Curves                           |              |           | |
| 4/19     | Blending | [Lab 10](https://bitbucket.org/msucsc441spring2016/labs/src/a06d55ef845ae4619253671d77eb0cb800b447ba/lab10/?at=master)             |   [Lab 9](https://bitbucket.org/msucsc441spring2016/labs/src/ab65b61a95f3ea4bba89b01f50aa83e53e77f11c/lab9/?at=master)        | |
| 4/21     | Particle Systems |              |           | |
| 4/26     | Review                                      |              |  [Lab 10](https://bitbucket.org/msucsc441spring2016/labs/src/a06d55ef845ae4619253671d77eb0cb800b447ba/lab10/?at=master)        | |
| 4/28     | Review                                      |              |  [Program 3](https://bitbucket.org/msucsc441spring2016/programs/src/fa1445b330fdd8b48c2d74b15f39eb04fc4d9617/program3/?at=master)          | |

### May

| Date     | Description                                 |
|----------|---------------------------------------------|
| 5/6      | Final - 12-1:50pm                           |


## Catalog Information

PREREQUISITE: M 221 and CSCI 232. High resolution computer graphics. 3D graphics
programming using a high level API. Vector mathematics for graphics. Graphics primitives.
Curve and surface representations. Transformations using matrices and quaternions.
Representing natural objects with particle systems and fractals. Shading and lighting models.
Global illumination models. Color representations.

## Course Offerings

At the end of the course, students should be able to
* Write basic graphical applications using OpenGL and C++
* Understand the modern graphics pipeline and how to leverage hardware to write
realtime graphics applications
* Use vectors and matrices to transform geometry in 3D space
* Understand common shading and lighting models for displaying 3D models
* Use particle systems to simulate real world phenomena.

## Graded Items

* Programs 50%
* Midterm + Final 25%
* Labs 25%

## Grading Policy
At the end of the semester, grades will be determined (after any curving takes place) based
on your class average as follows:

* 93+: A
* 90+: A
* 87+: B+
* 83+: B
* 80+: B
* 77+: C+
* 73+: C
* 70+: C
* 67+: D+
* 63: D
* 60: D-

you fall within one percentage point of the next grade higher, your grade on the final exam
will be examined. If it justifies you being in the next higher grade category, you will receive
that higher grade.

## Late Policy

Deadlines for all assignments will be noted in their descriptions. Turning in a project late will
result in severe deductions as follows:

* 20% within first 24 hours after deadline
* 40% within 48 hours
* 100% after 48 hours

However, you are allowed 2 late days throughout the semester that can be used on your
program assignments. You do not need to explain why you are using the days – these two
late days will be automatically applied to any late program assignments. After your two late
days have been used up, the late penalties apply.

## Collaboration Policy

All students should read the MSU Student Conduct Code.
When it comes to assignments, you may

* Work with the other people on your team if teams are allowed. Each assignment will
specify the maximum number of people per team.
* Share ideas with people in other teams.
* Help other teams troubleshoot problems.

You may NOT

* Share code you write with other teams.
* Submit code that someone on your team did not write.
* Modify another team's solution and claim it as your own.

Failure to abide by these rules will result in an "F" for the course and being reported to the
Dean of Students.